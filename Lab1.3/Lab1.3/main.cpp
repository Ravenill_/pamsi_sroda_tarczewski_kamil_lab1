#include "Palindrom.h"
#include <iostream>
#include <cstdlib>

int main()
{
	std::string str;

	std::cout << "Podaj wyraz do sprawdzenia, czy jest palindromem:\n";
	std::cin >> str;

	std::cout << "Wyraz " << str << " ";
	if (jestPal(str))
		std::cout << "jest palindromem\n";
	else
		std::cout << "NIE jest palindromem\n";

	system("PAUSE");
}