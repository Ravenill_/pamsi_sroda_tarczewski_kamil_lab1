#include "Stuff.h"

Stuff::Stuff()
{
}

int Stuff::Potega(int x, int p)
{
	if (p == 0)
		return 1;
	return Potega(x, (p - 1)) * x; //Wz�r: x^(p-1) * x = x^p, gdzie x^0 = 1
}

int Stuff::Silnia(int x)
{
	if (x == 1)
		return 1;
	return Silnia(x - 1) * x; //Wz�r: x! = (x-1)!*x
}

bool Stuff::check(int a, int b)
{
	if (b < 0)
		return false;
	else
		return true;
}