#include "stuff.h"
#include <iostream>
#include <cstdlib>

int main()
{
	Stuff App;
	int a, b;

	std::cout << "Podaj x i p, by wyliczyc x^p oraz p!\nPodaj x: ";
	std::cin >> a;
	std::cout << "Podaj p: ";
	std::cin >> b;

	if (!App.check(a, b))
	{
		std::cout << "Nie mozna wyliczyc ujemnej silni\n";
		system("PAUSE");
		return 0;
	}

	std::cout << "Potega: " << App.Potega(a, b);
	std::cout << "\nSilnia: " << App.Silnia(b) << "\n";
	system("PAUSE");
	return 0;
}