#pragma once

class CoreMulti
{
private:
	int **tab_dyn;
	int size1, size2; //Wymiary tablicy
	int range; //zakres liczb losowych
	int max; //MAX w tablicy
	bool read; //Czy wype�niono tablic�
	bool right; //czy dobrze zadklarowano rozmiar

public:
	CoreMulti();
	~CoreMulti();

	void run(int);
	void randomize(int);
	void getTab();
	void searchMax();


};