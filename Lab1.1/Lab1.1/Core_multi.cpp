#include "Core_multi.h"
#include <iostream>
#include <cstdlib>

CoreMulti::CoreMulti()
{
	size1 = 0;
	size2 = 0;
	range = 0;
	max = -1;
	read = 0;
	right = 1;

	//Warto�� tablicy
	do
	{
		if (!std::cin.good())
		{
			std::cin.clear();
			std::cin.sync();
			std::cin.ignore(1000, '\n');
		}
		system("CLS");
		std::cout << "Podaj dwa rozmiary tablicy dwuwymiarowej\n";
		std::cin >> size1 >> size2;
		right = 1;

		if (size1 <= 0 || size2 <= 0)
		{
			system("CLS");
			std::cout << "Error! Podano zly rozmiar tablicy!!!\n";
			system("PAUSE");
			right = 0;
		}

		if (!std::cin.good())
		{
			system("CLS");
			std::cout << "Error! Podano zly rozmiar tablicy!!!\n";
			system("PAUSE");
			right = 0;
		}
	} while (!right);

	//Kreacja tablicy
	tab_dyn = new int *[size1];
	for (int i = 0; i < size1; i++) tab_dyn[i] = new int [size2];
}

CoreMulti::~CoreMulti()
{
	for (int i = 0; i < size1; i++) delete[] tab_dyn[i];
	delete[] tab_dyn;
	tab_dyn = NULL;
}

void CoreMulti::run(int opt)
{
	switch (opt)
	{
	case 1: //Losowanie
		system("CLS");
		std::cout << "Podaj x w zakresie losowania od 0 do x:\n";
		std::cin >> range;
		randomize(range);
		break;

	case 2: //Wy�wietlanie
		if(read)
			getTab();
		else
		{
			system("CLS");
			std::cout << "Nie wygenerowano tablicy. Wybierz opcje [1]\n";
			system("PAUSE");
		}
		break;

	case 3://Wyszukiwanie MAX
		if(read)
			searchMax();
		else
		{
			system("CLS");
			std::cout << "Nie wygenerowano tablicy. Wybierz opcje [1]\n";
			system("PAUSE");
		}
		break;
	}
}

void CoreMulti::randomize(int a)
{
	system("CLS");

	if (a <= 0)
	{
		std::cout << "Nalezy podac zakres liczb dodatnich\n";
		system("PAUSE");
		return;
	}

	for (int i = 0; i < size2; i++)
	{
		for (int j = 0; j < size1; j++)
		{
			tab_dyn[j][i] = (std::rand() % (a + 1)); //Wylosowanie liczby z zakresu 0 - x
			std::cout << tab_dyn[j][i] << "  ";
		}
		std::cout << "\n";
	}
	read = true;//usuni�cie blokady z opcji 2,3
	max = -1;//reset maxa
	system("PAUSE");
}

void CoreMulti::getTab()
{
	system("CLS");
	for (int i = 0; i < size2; i++)
	{
		for (int j = 0; j < size1; j++)
			std::cout << tab_dyn[j][i] << "  ";

		std::cout << "\n";
	}
	system("PAUSE");
}

void CoreMulti::searchMax()
{
	system("CLS");
	if (max == -1)
	{
		for (int i = 0; i < size2; i++)
		{
			for (int j = 0; j < size1; j++)
			{
				if (max < tab_dyn[j][i])
					max = tab_dyn[j][i];//Przypisanie warto�ci do MAX, je�li wi�kszy od dotychczasowego
			}
		}
	}

	std::cout << "Wartosc maksymalna znaleziona w tablicy to:\n" << max << std::endl;
	system("PAUSE");
}

