#include "Core_single.h"
#include <iostream>

CoreSingle::CoreSingle()
{
	read = 0;
	size1 = 0;
	size2 = 0;

	r.open("read_text.txt", std::ios::in);
	rb.open("read_bin.bin", std::ios::in | std::ios::binary);
	w.open("write_text.txt", std::ios::out);
	wb.open("write_bin.bin", std::ios::out | std::ios::binary);

	if (!r.good() || !rb.good() || !w.good() || !wb.good())
		std::cout << "Masz problem\n";

	r >> size1;
	rb.read(reinterpret_cast<char *>(&size2), sizeof size2);

	tab_single_t = new int [size1];
	tab_single_b = new int[size2];
}

CoreSingle::~CoreSingle()
{
	delete[] tab_single_b;
	delete[] tab_single_t;
	tab_single_b = NULL;
	tab_single_t = NULL;
	r.close();
	rb.close();
	w.close();
	wb.close();
}

void CoreSingle::run(int opt)
{
	switch (opt)
	{
	case 1:
		reading();
		break;

	case 2:
		if (read)
			writing();
		else
		{
			system("CLS");
			std::cout << "Nie wyczytano tablicy. Wybierz opcj� [1] lub [3]\n";
			system("PAUSE");
		}
		break;

	case 3:
		readingBinary();
		break;

	case 4:
		if (read)
			writingBinary();
		else
		{
			system("CLS");
			std::cout << "Nie wyczytano tablicy. Wybierz opcj� [1] lub [3]\n";
			system("PAUSE");
		}
		break;
	}
}

void CoreSingle::reading()
{
	for (int i = 0; i < size1; i++)  r >> tab_single_t[i];
	read = true;
}

void CoreSingle::writing()
{
	w << size1 << "\t";
	for (int i = 0; i < size1; i++)  w << tab_single_t[i] << " ";
}

void CoreSingle::readingBinary()
{
	for (int i = 0; i < size2; i++)  rb.read(reinterpret_cast<char *>(& tab_single_b[i]), sizeof tab_single_b[i]);
	read = true;
}

void CoreSingle::writingBinary()
{
	wb.write(reinterpret_cast<const char *>(& size2), sizeof size2);
	for (int i = 0; i < size2; i++)  wb.write(reinterpret_cast<const char *>(& tab_single_b[i]), sizeof tab_single_b[i]);
}