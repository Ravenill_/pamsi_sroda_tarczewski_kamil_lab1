#include "Displayer.h"
#include <cstdlib>
#include <iostream>

using namespace std;

void getMenu()
{
	system("CLS");
	cout << "--MENU GLOWNE--\n";
	cout << "[1] TABLICA DWUWYMIAROWA\n";
	cout << "[2] TABLICA JEDNOWYMIAROWA\n";
	cout << "[0] UCIECZKA Z PROGRAMU\n";
}

void getMenu1()
{
	system("CLS");
	cout << "--MENU TABLICY DWUWYMIAROWEJ--\n";
	cout << "[1] Wypelnij tablice losowa zawartoscia\n";
	cout << "[2] Wyswietl tablice\n";
	cout << "[3] Znajdz wartosc MAX\n";
	cout << "[0] Powrot\n";
}

void getMenu2()
{
	system("CLS");
	cout << "--MENU TABLICY JEDNOWYMIAROWEJ--\n";
	cout << "[1] Wczytaj z pliku tekstowego\n";
	cout << "[2] Zapisz do pliku tekstowego\n";
	cout << "[3] Wczytaj z pliku binarnego\n";
	cout << "[4] Zapisz do pliku binarnego\n";
	cout << "[0] Powrot\n"; 
}
