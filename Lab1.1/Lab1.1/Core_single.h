#pragma once
#include <fstream>

class CoreSingle
{
private:
	std::ifstream r; //Wczytywanie
	std::ofstream w; //Zapisywanie
	std::ifstream rb; //Wczytywanie binarnych
	std::ofstream wb; //Zapisywanie binarnych
	int *tab_single_b; //binarna
	int *tab_single_t; //tekstowa
	int size1, size2; //rozmiary talbic
	bool read;

public:
	CoreSingle();
	~CoreSingle();

	void run(int);

	void reading();
	void writing();
	void readingBinary();
	void writingBinary();
};
