#pragma once
#include "Core_multi.h"
#include "Core_single.h"

enum Stat { MENU, MENU1, MENU2, END, RUN_CORE1, RUN_CORE2 };

class App
{
private:
	int option;
	Stat state;
	CoreMulti multi; //opcje z tab dwuwymiarową CORE1
	CoreSingle single; //opcje z tab jednowymiarową CORE2


public:
	App();
	~App();

	void run();
	void menu(); //główne
	void menu1(); //jednowymiarowa
	void menu2(); //dwuwymiarowa

	void run_multi(); //CORE 1
	void run_single(); //CORE 2
};