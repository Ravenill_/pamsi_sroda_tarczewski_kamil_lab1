#include "App.h"
#include "Displayer.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

App::App()
{
	state = MENU; //Ustawienie zmiennej informuj�cej gdzie si� znajdujemy na MENU
	option = 0;

	srand(static_cast<int>(time(NULL))); //Randomizer
}

App::~App()
{

}

void App::run()
{
	//obs�uga programu
	while (state != END)
	{
		switch (state)
		{
		case Stat::MENU:
			menu();
			break;

		case Stat::MENU1:
			menu1();
			break;

		case Stat::MENU2:
			menu2();
			break;

		case Stat::RUN_CORE1: //Tablica dynamiczna dwuwymiarowa
			run_multi();
			break;

		case Stat::RUN_CORE2: //Tablica jednowymiarowa dynamiczna
			run_single();
			break;

		default:

			break;
		}
	}
}

void App::menu()
{
	int opt;

	while (state == MENU)
	{
		//wy�wietlenie menu
		getMenu();

		//odczytanie opcji, g�upkoodporno��
		std::cin >> opt;
		if (!std::cin.good())
		{
			system("CLS");
			std::cerr << "Error! Wpisano zla opcje\n";
			std::cin.clear();
			std::cin.sync();
			std::cin.ignore(1000, '\n');
			system("PAUSE");
		}
		else
		{
			switch (opt)
			{
			case 1:
				state = MENU1;
				break;

			case 2:
				state = MENU2;
				break;

			case 0:
				state = END;
				break;

			default:
				system("CLS");
				std::cout << "Error! Wpisano zla opcje\n";
				system("PAUSE");
				break;
			}//G��wna obs�uga menu
		}//Else
	}//While
}

//Menu tab dwuwymiarowej
void App::menu1()
{
	int opt;

	while (state == MENU1)
	{
		//wy�wietlenie menu
		getMenu1();

		//odczytanie opcji, g�upkoodporno��
		std::cin >> opt;
		if (!std::cin.good())
		{
			system("CLS");
			std::cerr << "Error! Wpisano zla opcje\n";
			std::cin.clear();
			std::cin.sync();
			std::cin.ignore(1000, '\n');
			system("PAUSE");
		}
		else
		{
			switch (opt)
			{
			case 1:
				state = RUN_CORE1;
				option = opt;
				break;

			case 2:
				state = RUN_CORE1;
				option = opt;
				break;

			case 3:
				state = RUN_CORE1;
				option = opt;
				break;

			case 0:
				state = MENU;
				break;

			default:
				system("CLS");
				std::cout << "Error! Wpisano zla opcje\n";
				system("PAUSE");
				break;
			}//G��wna obs�uga menu
		}//Else
	}//While
}

//Menu tab jednowymiarowej
void App::menu2()
{
	int opt;

	while (state == MENU2)
	{
		//wy�wietlenie menu
		getMenu2();

		//odczytanie opcji, g�upkoodporno��
		std::cin >> opt;
		if (!std::cin.good())
		{
			system("CLS");
			std::cerr << "Error! Wpisano zla opcje\n";
			std::cin.clear();
			std::cin.sync();
			std::cin.ignore(1000, '\n');
			system("PAUSE");
		}
		else
		{
			switch (opt)
			{
			case 1:
				state = RUN_CORE2;
				option = opt;
				break;

			case 2:
				state = RUN_CORE2;
				option = opt;
				break;

			case 3:
				state = RUN_CORE2;
				option = opt;
				break;

			case 4:
				state = RUN_CORE2;
				option = opt;
				break;

			case 0:
				state = MENU;
				break;

			default:
				system("CLS");
				std::cout << "Error! Wpisano zla opcje\n";
				system("PAUSE");
				break;
			}//G��wna obs�uga menu
		}//Else
	}//While
}

//Modu� z funkcjami dla tablicy dwuwymiarowej
void App::run_multi()
{
	multi.run(option);
	option = 0;
	state = MENU1;
}

//Modu� dla tablicy jednowymiarowej
void App::run_single()
{
	single.run(option);
	option = 0;
	state = MENU2;
}

